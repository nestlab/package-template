import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { PackageModule } from '../src';
import { PackageService } from '../src/services/package.service';

describe('PackageModule', () => {
    let app: INestApplication;

    beforeAll(async () => {
        const testingModule = await Test.createTestingModule({
            imports: [
                PackageModule.forRoot({}),
            ],
        }).compile();

        app = testingModule.createNestApplication();
    });

    test('Test hello', () => {
        const service = app.get(PackageService);
        const value = service.hello();
        expect(value).toBe('Hello world');
    });
});
