import { DynamicModule, HttpModule, Module, Provider } from '@nestjs/common';
import { PackageService } from './services/package.service';

@Module({})
export class PackageModule {
    static forRoot(options: any): DynamicModule {
        const providers: Provider[] = [
            PackageService,
        ];

        return {
            module: PackageModule,
            imports: [HttpModule],
            providers,
            exports: providers,
        };
    }
}
